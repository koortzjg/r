Package: litsearchr
Type: Package
Title: Automated Search Term Selection and Search Strategy for
        Systematic Reviews
Version: 1.0.0
Authors@R: c(
  person(
    given = "Eliza",
    family = "Grames",
    role = c("aut", "cre"),
    email = "eliza.grames@uconn.edu",
    comment = c(ORCID = "0000-0003-1743-6815")),
  person(
    given = "Andrew",
    family = "Stillman",
    role = c("aut"),
    email = "andrew.stillman@uconn.edu",
    comment = c(ORCID = "0000-0001-6692-380X")),
  person(
    given = "Morgan",
    family = "Tingley",
    role = c("aut"),
    email = "mtingley@g.ucla.edu",
    comment = c(ORCID = "0000-0002-1477-2218")),
  person(
    given = "Chris",
    family = "Elphick",
    role = c("aut"),
    email = "chris.elphick@uconn.edu")    
    )
URL: http://elizagrames.github.io/litsearchr
BugReports: https://github.com/elizagrames/litsearchr/issues
Description: Systematic review, meta-analysis, and other forms of evidence 
    synthesis require researchers to identify the body of evidence relevant 
    to their research question. Partially automating evidence synthesis 
    would reduce the amount of human time and effort needed to conduct 
    a systematic review and reduce bias in keyword selection when researchers 
    develop search strategies. This package facilitates quick, objective, 
    reproducible search strategy development using text-mining and 
    keyword co-occurrence networks to identify important terms to include 
    in a search strategy as described in Grames et al. (2019) 
    <doi:10.1111/2041-210X.13268>. It can automatically write Boolean 
    search strings in up to 53 different languages, with stemming support 
    for English. To assess the quality of a search, it can also check the 
    results of a search against a set of known, relevant articles to get 
    performance metrics.
License: GPL-3
LazyData: true
RoxygenNote: 7.1.0
Imports: changepoint, SnowballC, utils, igraph, ngram, synthesisr (>=
        0.3), stopwords, tm
Suggests: xml2, translate, knitr, rmarkdown
VignetteBuilder: knitr
Depends: R (>= 2.10)
Date: 2020-07-20
RemoteType: github
RemoteHost: api.github.com
RemoteRepo: litsearchr
RemoteUsername: elizagrames
RemoteRef: main
RemoteSha: 0c108e30f03c773123da2e6fe3f7cb6581d7c523
GithubRepo: litsearchr
GithubUsername: elizagrames
GithubRef: main
GithubSHA1: 0c108e30f03c773123da2e6fe3f7cb6581d7c523
NeedsCompilation: no
Packaged: 2023-04-18 09:25:29 UTC; Hardi Koortzen
Author: Eliza Grames [aut, cre] (<https://orcid.org/0000-0003-1743-6815>),
  Andrew Stillman [aut] (<https://orcid.org/0000-0001-6692-380X>),
  Morgan Tingley [aut] (<https://orcid.org/0000-0002-1477-2218>),
  Chris Elphick [aut]
Maintainer: Eliza Grames <eliza.grames@uconn.edu>
Built: R 4.2.3; ; 2023-04-18 09:25:29 UTC; windows
